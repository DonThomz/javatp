package com.company;

public class Toutou {
    String n;
    int np;

    Toutou(String n, int np) throws IllegalArgumentException{
        if(n == null) throw new IllegalArgumentException("Nom null");
        else this.n = n;
        if(np < 0 ) throw new IllegalArgumentException("Nombre de puce négatif");
        else this.np = np;
    }
}
