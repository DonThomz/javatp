package com.company;

// Parent class

public class Entreprise {

    // Attributes

    private String nom;
    private int capital;
    private int numberEmployers;
    private String mission;

    // Constructor

    protected Entreprise(String nom, int capital, int numberEmployers, String mission){
        this.nom = nom;
        this.capital = capital;
        this.numberEmployers = numberEmployers;
        this.mission = mission;
    }

    // Methods

    public String mission() throws SecretMissionException{
        if(mission.isEmpty()) throw new SecretMissionException("Le champ mission est vide");
        return this.mission;
    }

    public int capital() throws NomProfitException{
        if(capital == 0) throw new NomProfitException("Le profit est nul");
        return this.capital;
    }

    // Get

    public int getCapital() {
        return capital;
    }

    public String getMission() {
        return mission;
    }

    public String getNom() {
        return nom;
    }
}

// Extends class

class EntrepriseSecret extends Entreprise{

    protected EntrepriseSecret(String nom, int capital, int numberEmployers, String mission) {
        super(nom, capital, numberEmployers, mission);
    }

    public String mission() throws SecretMissionException{
        throw new SecretMissionException("La mission est vide");
    }
}

class EntrepriseSansProfit extends Entreprise{

    protected EntrepriseSansProfit(String nom, int capital, int numberEmployers, String mission) {
        super(nom, capital, numberEmployers, mission);
    }

    public int capital() throws NomProfitException{
        throw  new NomProfitException("Le profit est nul");
    }
}


// Exceptions

class SecretMissionException extends Exception{
    public SecretMissionException(String message){
        super(message);
    }
}

class NomProfitException extends Exception{
    public NomProfitException(String message){
        super(message);
    }

}

