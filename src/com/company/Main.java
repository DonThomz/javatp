package com.company;

import java.util.*;

public class Main {

    public static Scanner keyboard = new Scanner(System.in);

    public static void main(String[] args) throws NomProfitException, SecretMissionException {

        loadMenu();

    }

    public static void loadMenu() throws SecretMissionException, NomProfitException {
        int choice = 0;

        do {
            System.out.println("" +
                    "Choisir un exercice dans la liste : " +
                    "\nPartie I Classe scanner " +
                    "\n\t1) Exo 1" +
                    "\n\t2) Exo 2 " +
                    "\nPartie II Classe interne" +
                    "\n\t3) Exo 1" +
                    "\nPartie III Exceptions" +
                    "\n\t4) Exo 1" +
                    "\n\t5) Exo 2" +
                    "\nQUIITER : 0");

            while(!keyboard.hasNextInt()){
                String input = keyboard.next();
                System.out.printf("\"%s\" n'est pas valide.\n", input);
            }
            choice = keyboard.nextInt();


            switch (choice) {
                case 1:
                    exo1();
                    promptEnterKey();
                    clearScreen();
                    break;
                case 2:
                    exo2();
                    promptEnterKey();
                    clearScreen();
                    break;
                case 3:
                    exo3();
                    promptEnterKey();
                    clearScreen();
                    break;
                case 4:
                    exo4();
                    promptEnterKey();
                    clearScreen();
                    break;
                case 5:
                    exo5();
                    promptEnterKey();
                    clearScreen();
                    break;
                default:
                    break;
            }


        } while (choice != 0);

    }

    /** EXO 1
        Ecrire  un  programme  prenant  un  nombre  sur  l'entrée  standard  et  affichant  celui-ci.
        Pour  cela,  on utilisera un objet (une instance) de classe Scanner et particulièrement sa méthode nextInt()
     */
    public static void exo1(){
        System.out.println("Ecrire un nombre");
        while(!keyboard.hasNextInt()){
            String input = keyboard.next();
            System.out.printf("\"%s\" n'est pas un chiffre valide.\n", input);
        }
        int result = keyboard.nextInt();
        System.out.println("Result = " + result);
    }

    /** EXO 2
     *  Modifier le programme précédent afin qu’il lise au clavier une suite de nombres réels positifs ou nuls (correspondant à des notes),
     *  terminée par la valeur -1, et calcule la moyenne olympique de ces valeurs, c'est à dire la moyenne des notes sans prendre en compte la note la plus élevée ni la note la moins élevée.
     */
    public static void exo2(){

        ArrayList<Integer> notes = new ArrayList<>();
        int result;
        int min, max;
        double mean = 0;

        while(true){
            System.out.println("Ecrire une note ou taper -1 pour quitter");
            while (!keyboard.hasNextInt()) {
                String input = keyboard.next();
                System.out.printf("\"%s\" n'est pas valide.\n", input);
            }
            result = keyboard.nextInt();
            if(result != -1)
                notes.add(result);
            else break;
        }
        // if notes size is greater to 3
        if( notes.size() >= 3 ) {

            // deep copy
            ArrayList<Integer> tmp_notes = new ArrayList<>();
            Iterator<Integer> iterator = notes.iterator();

            while(iterator.hasNext())
            {
                //Add the object clones
                tmp_notes.add(iterator.next());
            }

            // get max and min
            tmp_notes.remove(Collections.min(tmp_notes));
            tmp_notes.remove(Collections.max(tmp_notes));

            // compute mean
            for (Integer i: tmp_notes
            ) {
                mean = mean + i;
            }
            mean = mean / tmp_notes.size();
            System.out.println("Moyenne olympique de " + notes + " = " + mean);
        }
        else{
            for (Integer i: notes
                 ) {
                mean = mean + i;
            }
            mean = mean / notes.size();
            System.out.println("Moyenne olympique de " + notes + " = " + mean);
        }
    }

    /** EXO 3
     *
     * */
    public static void exo3(){

        // Test 1 => 4WD 4 portes + coffre
        Vehicule v1 = new Vehicule("BMW", "4WD");
        Vehicule.Porte p1 = new Vehicule.Porte(4, true);
        System.out.println(v1);
        System.out.println("Total portes = " + p1.getTotalPortes());

        // Test 2 => Crysler 2 portes pas de coffre
        Vehicule v2 = new Vehicule("BMW", "Crysler");
        Vehicule.Porte p2 = new Vehicule.Porte(2, false);
        System.out.println(v2);
        System.out.println("Total portes = " + p2.getTotalPortes());
    }

    /**
     *  EXO 4
     */
    public static void exo4() throws NomProfitException, SecretMissionException {
        Entreprise e = new Entreprise("Entreprise", 100, 100, "Acheter des ordinateurs");


        EntrepriseSecret es = new EntrepriseSecret("Entreprise Secret", 5, 50, "");
        //es.mission();

        EntrepriseSansProfit ess = new EntrepriseSansProfit("Entreprise Sans profit", 0, 50, "");
        //ess.capital();

        Entreprise ford = new Entreprise("Ford", 1248822, 1000, "Mission de ford");
        Entreprise CIA = new Entreprise("CIA", 1000000, 10000, "Mission du CIA");
        Entreprise spectre = new Entreprise("Spectre", 52, 570, "Mission de spectre");
        Entreprise croixRouge = new Entreprise("Croix Rouge", 523842, 3000, "Mission de la Croix Rouge");
        Entreprise microsoft = new Entreprise("Microsoft", 212000051, 50000, "Mission microsoft");
        Entreprise efrei = new Entreprise("Efrei", 20, 300, "");


        Entreprise[] enterprises = {ford, CIA, spectre, croixRouge, microsoft, efrei};
        getEnterprisesCapital(enterprises);
    }

    public static void getEnterprisesCapital(Entreprise[] enterprises) throws NomProfitException, SecretMissionException {
        for (Entreprise e: enterprises
             ) {
            System.out.println(e.getNom());
            System.out.println("\tCapital : " + e.capital());
            System.out.println("\tMission : " + e.mission());
        }
    }

    /**
     *  EXO 5
     */
    public static void exo5() throws IllegalArgumentException{
        Toutou milou = new Toutou("milou", 4);
        Toutou medor = new Toutou("medor", -11);
    }


    public static void promptEnterKey(){
        System.out.println("Press \"ENTER\" to continue...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
