package com.company;

public class Vehicule {

    // attributes
    private String vehiType;
    private String vehicNom;
    private Moteur moteur;

    Vehicule(String vehicNom, String vehiType){
        this.vehicNom = vehicNom;
        this.vehiType = vehiType;
        this.moteur = new Moteur();
    }

    protected class Moteur{
        private String moteurType;
        public Moteur(){
            if(vehiType != null){
                switch (vehiType){
                    case "4WD":
                        this.moteurType = "Grand";
                        break;
                    case "Crysler":
                        this.moteurType = "Petit";
                        break;
                    default:
                        this.moteurType = "";
                }
            }
        }

        @Override
        public String toString() {
            return "Moteur{" +
                    "moteurType='" + moteurType + '\'' +
                    '}';
        }
    }

    public static class Porte{
        int portePassager;
        boolean coffre;

        public Porte(int portePassager, boolean coffre){
            this.portePassager = portePassager;
            this.coffre = coffre;
        }

        public int getTotalPortes() {

            // impossible de récupérer les attribues de Véhicule car ils sont non static

            if(coffre)
                return 1 + this.portePassager;
            else
                return this.portePassager;
        }
    }


    @Override
    public String toString() {
        return "Vehicule{" +
                "vehiNom='" + vehicNom + '\'' +
                ", vehiType='" + vehiType + '\'' +
                ", moteur=" + moteur +
                '}';
    }
}
